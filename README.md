# ![le logo de Framasoft](mathscape/escape/static/images/logo.png) MATH'SCAPE - Installation ![le logo de Framasoft](mathscape/escape/static/images/logo.png)


### Introduction
Math'Scape est une plateforme web permettant à des professeurs de créer des escapes games de toute pièce ayants pour but d'apporter un côté ludique à l'apprentissage. Math'Scape offre des interfaces de gestion simple et efficaces : autant pour les étudiants que pour les professeurs.

###### Note des développeurs
L'apllication ayant été développée sous le sytème `Linux - Ubuntu`, le manuel ci-contre explique la démarche à suivre sur un même système. il n'est pas assuré que cela fonctionne sur tout autre sytème.

### Téléchargement
Depuis le dépôt [Gitlab](https://gitlab.com/DENECHERE_Killian/escape-game), téléchargez le projet sous forme d'archive `.zip`.
Décompressez l'archive obtenu et un dossier `escape-game-master` apparaitra.

### Prérequis au lancement
Si vous avez déjà lancé l'application auparavant, vous devez pouvoir passer à la section suivante : **[Lancement](#lancement)**.

Si vous lancez l'application pour la première fois, il vous faut installer les modules nécéssaires au lancement de l'apllication.
Déplacez-vous dans le dossier `escape-game-master` et ouvrez-y un terminal. Tapez la commande suivante :
```bash
source venv.sh
```
Après un certain temps, un message s'affichera : `Voulez-vous lancer l'application ?`

| Premier lancement | Autres lancements |
| :-: | :-: |
| Taper sur la touche `Enter`ou `Entrée` de votre clavier | Tapez sur la touche `y` puis sur la touche `Enter`ou `Entrée` de votre clavier (si vous voulez lancer l'application dans firefox)|

### Lancement
Si vous n'avez pas encore tapé la commande:
```bash
source venv.sh
```
Regardez la section précédente : **[Prérequis au lancement](#prérequis-au-lancement)**.

---
Bien, maintenant que votre environement virtuel est lancé et que les dépendances sont installées, nous pouvons passer à la suite !
###### Note des développeurs
Si vous avez déjà lancé l'application une fois (et que vous ne voulez pas recréer la base de données ou effectuer des insertions automatique dans celle-ci, vous pouvez passer à la section **[Lancer l'application](#lancer-lapplication)**).
Sachez qu'en cas de problème, faire un `flask launch` ou un `flask syncdb` supprime la BD (si existante) et la remplace.

##### Créer la base de données
Pour utiliser l'application, il faut créer `la base de données`, ce n'est pas long vous allez voir : plusieurs méthodes sont disponibles ! En fonction de votre besoin, suivez les colonnes : elles indiquent les commandes à taper dans le terminal que vous avez du ouvrir plus tôt.

| Automatique (recommandé pour le premier lacement) | Manuel (recommandé en cas de soucis) |
| :-: | :-: |
| `flask launch` (créé la BD et insère des valeurs d'exemple) | `flask syncdb` (créé uniquement la BD) |
| - | `flask loadsample` (insère des valeurs d'exemple dans la BD) |

##### Lancer l'application
Ça y est, dernière étape, tapez la commande suivante pour lacer le serveur sur lequel l'application est "hébergée" :
```bash
flask run
```
Rendez-vous alors dans votre navigateur favoris à l'adresse [localhost:5000](localhost:5000).

### Arrêt
Dans le terminal depuis lequel l'application est lancée (son serveur), tapez succesivement sur les touches:
`Ctrl`+`c`
Fermez le terminal et c'est tout !

------
------
**Rédacteur**
- Denéchère Killian *(chef de projet)*

**Équipe de réalision**
* Denéchère Killian *(chef de projet)*
*  Regneau Alfred
* Duprez Thibaud
* Georget Kevin
* Mauger Enzo
* Carneiro Erwan
* Bertrand Hugo

**Contexte de réalistion**
- Projet de seconde année de DUT Informatique *(2019 - 2020)*
