"""
Ce fichier contient les fonctions qui vont aller chercher, dans la BD,
les informations nécéssaires aux vues
"""
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import AnonymousUserMixin
from .app import DB


class AssociationGroupeIndice(DB.Model):
    """
    Relation Many To Many Indice/Groupe
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_groupe = DB.Column(DB.Integer, DB.ForeignKey('groupe.id'))
    id_indice = DB.Column(DB.Integer, DB.ForeignKey('indice.id'))

    groupe = DB.relationship("Groupe", backref="associations_groupe_indice")
    indice = DB.relationship("Indice", backref="associations_groupe_indice")


class AssociationGroupeSalle(DB.Model):
    """
    Relation Many To Many Groupe/Salle
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    est_decouverte = DB.Column(DB.Boolean)
    id_groupe = DB.Column(DB.Integer, DB.ForeignKey('groupe.id'))
    id_salle = DB.Column(DB.Integer, DB.ForeignKey('salle.id'))

    groupe = DB.relationship("Groupe", backref="associations_groupe_salle")
    salle = DB.relationship("Salle", backref="associations_groupe_salle")


class AssociationGroupeZone(DB.Model):
    """
    Relation Many To Many Zone/Groupe
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_groupe = DB.Column(DB.Integer, DB.ForeignKey('groupe.id'))
    id_zone = DB.Column(DB.Integer, DB.ForeignKey('zone.id'))

    groupe = DB.relationship("Groupe", backref="associations_groupe_zone")
    zone = DB.relationship("Zone", backref="associations_groupe_zone")


class AssociationPersonneGroupe(DB.Model):
    """
    Relation Many To Many Personne/Groupe
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_personne = DB.Column(DB.Integer, DB.ForeignKey('personne.id'))
    id_groupe = DB.Column(DB.Integer, DB.ForeignKey('groupe.id'))

    personne = DB.relationship("Personne", backref="associations_personne_groupe")
    groupe = DB.relationship("Groupe", backref="associations_personne_groupe")


class AssociationIndiceEnigme(DB.Model):
    """
    Relation Many To Many Indice/Enigme
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_indice = DB.Column(DB.Integer, DB.ForeignKey('indice.id'))
    id_enigme = DB.Column(DB.Integer, DB.ForeignKey('enigme.id'))

    indice = DB.relationship("Indice", backref="associations_indice_enigme")
    enigme = DB.relationship("Enigme", backref="associations_indice_enigme")


class AssociationEnigmeGroupe(DB.Model):
    """
    Relation Many To Many Enigme/Groupe
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_enigme = DB.Column(DB.Integer, DB.ForeignKey('enigme.id'))
    id_groupe = DB.Column(DB.Integer, DB.ForeignKey('groupe.id'))

    enigme = DB.relationship("Enigme", backref="associations_enigme_groupe")
    groupe = DB.relationship("Groupe", backref="associations_enigme_groupe")


class AssociationModeleEnigmeModeleSalle(DB.Model):
    """
    Relation Many To Many ModeleEnigme/Salle
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_modele_enigme = DB.Column(DB.Integer, DB.ForeignKey('modele_enigme.id'))
    id_modele_salle = DB.Column(DB.Integer, DB.ForeignKey('modele_salle.id'))

    modele_enigme = DB.relationship("ModeleEnigme", backref="associations_modele_enigme_modele_salle")
    modele_salle = DB.relationship("ModeleSalle", backref="associations_modele_enigme_modele_salle")


class AssociationModeleIndiceModeleSalle(DB.Model):
    """
    Relation Many To Many ModeleIndice/ModeleSalle
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_modele_indice = DB.Column(DB.Integer, DB.ForeignKey('modele_indice.id'))
    id_modele_salle = DB.Column(DB.Integer, DB.ForeignKey('modele_salle.id'))

    modele_indice = DB.relationship("ModeleIndice", backref="associations_modele_indice_modele_salle")
    modele_salle = DB.relationship("ModeleSalle", backref="associations_modele_indice_modele_salle")

class AssociationEnigmeSpecialeSalle(DB.Model):
    """
    Relation Many To Many EnigmeSpeciale/Salle
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_enigme_speciale = DB.Column(DB.Integer, DB.ForeignKey('enigme_speciale.id'))
    id_salle = DB.Column(DB.Integer, DB.ForeignKey('salle.id'))

    enigme_speciale = DB.relationship("EnigmeSpeciale", backref="associations_enigme_speciale_salle")
    salle = DB.relationship("Salle", backref="associations_enigme_speciale_salle")

class AssociationGroupeEnigmeSpeciale(DB.Model):
    """
    Relation Many To Many Groupe/EnigmeSpeciale
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_enigme_speciale = DB.Column(DB.Integer, DB.ForeignKey('enigme_speciale.id'))
    id_groupe = DB.Column(DB.Integer, DB.ForeignKey('groupe.id'))

    enigme_speciale = DB.relationship("EnigmeSpeciale", backref="associations_groupe_enigme_speciale")
    groupe = DB.relationship("Groupe", backref="associations_groupe_enigme_speciale")

class AssociationGroupeIndiceSpeciale(DB.Model):
    """
    Relation Many To Many Groupe/EnigmeSpeciale
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_indice_speciale = DB.Column(DB.Integer, DB.ForeignKey('indice_speciale.id'))
    id_groupe = DB.Column(DB.Integer, DB.ForeignKey('groupe.id'))

    indice_speciale = DB.relationship("IndiceSpeciale", backref="associations_groupe_indice_speciale")
    groupe = DB.relationship("Groupe", backref="associations_groupe_indice_speciale")

class AssociationZoneIndiceSpeciale(DB.Model):
    """
    Relation Many To Many Zone/IndiceSpeciale
    """
    __mapper_args__ = {'confirm_deleted_rows': False}

    id = DB.Column(DB.Integer, primary_key=True)
    id_indice_speciale = DB.Column(DB.Integer, DB.ForeignKey('indice_speciale.id'))
    id_zone = DB.Column(DB.Integer, DB.ForeignKey('zone.id'))

    indice_speciale = DB.relationship("IndiceSpeciale", backref="associations_zone_indice_speciale")
    zone = DB.relationship("Zone", backref="associations_zone_indice_speciale")

# class AssociationJeuVariableVariable(DB.Model):
#     """
#     Relation Many To Many JeuVariable/Variable
#     """
#     id = DB.Column(DB.Integer, primary_key=True)
#     id_jeu_variable = DB.Column(DB.Integer, DB.ForeignKey('jeu_variable.id'))
#     id_variable = DB.Column(DB.Integer, DB.ForeignKey('variable.id'))
#
#     jeu_variable = DB.relationship("JeuVariable", backref="associations_jeu_variable_variable")
#     variable = DB.relationship("Variable", backref="associations_jeu_variable_variable")


class Groupe(DB.Model):
    """
    Table Groupe
    """
    id = DB.Column(DB.Integer, primary_key=True)
    nom = DB.Column(DB.String(50))

    #Relation de Groupe avec Enigme (Many To Many)
    enigmes = DB.relationship("Enigme", secondary="association_enigme_groupe")

    #Relation de Groupe avec EnigmeSpeciale(Many To Many)
    enigmes_speciales = DB.relationship("EnigmeSpeciale", secondary="association_groupe_enigme_speciale")

    #Relation de Groupe avec IndiceSpeciale (Many To Many)
    indices_speciales = DB.relationship("IndiceSpeciale", secondary="association_groupe_indice_speciale")

    #Relation de Groupe avec Personne (Many To Many)
    personnes = DB.relationship("Personne", secondary="association_personne_groupe")

    #Relation de Groupe avec Zone (Many To Many)
    zones = DB.relationship("Zone", secondary="association_groupe_zone")

    #Relation de Groupe avec Indice (Many To Many)
    indices = DB.relationship("Indice", secondary="association_groupe_indice")

    #Relation de Groupe avec Salle (Many To Many)
    salles = DB.relationship("Salle", secondary="association_groupe_salle")

    def __repr__(self):
        """
        Exemple de représentation :
        Groupe n°1 : 2A22
        """
        return "Groupe n°%d : %s" % (self.id, self.nom)


class Theme(DB.Model):
    """
    Table Theme
    """
    id = DB.Column(DB.Integer, primary_key=True)
    nom = DB.Column(DB.String(50), unique=True)

    #Relation de Theme avec EnigmeSpeciale (Many To One)
    enigmes_speciales = DB.relationship("EnigmeSpeciale", back_populates="theme")

    def __repr__(self):
        """
        Exemple de représentation :
        Theme n°1 : Proba
        """
        return "Theme n°%d : %s" % (self.id, self.nom)


class Image(DB.Model):
    """
    Table Image
    """
    id = DB.Column(DB.Integer, primary_key=True)
    image = DB.Column(DB.String(2000000))
    pour_enigme_speciale = DB.Column(DB.Boolean, default=False)

    # Relation de Image avec ModeleSalle (One To One)
    modele_salle = DB.relationship("ModeleSalle", back_populates="image")

    # Relation de Image avec ModeleEnigme (One To One)
    modele_enigme = DB.relationship("ModeleEnigme", back_populates="image")

    # Relation de Image avec ModeleIndice (One To One)
    modele_indice = DB.relationship("ModeleIndice", back_populates="image")

    # Relation de Image avec IndiceSpecial (One To One)
    indice_speciale = DB.relationship("IndiceSpeciale", back_populates="image")

    # Relation de Image avec EnigmeSpecial (One To One)
    enigme_speciale = DB.relationship("EnigmeSpeciale", back_populates="image")

    # Relation de Image avec Personne (Many To One)
    personne = DB.relationship("Personne", back_populates="image")

class Difficulte(DB.Model):
    """
    Table Difficulte
    """
    id = DB.Column(DB.Integer, primary_key=True)
    nom = DB.Column(DB.String(50), unique=True)

    # Relation de Difficulte avec Enigme Speciale (Many To One)
    enigmes_speciales = DB.relationship("EnigmeSpeciale", back_populates="difficulte")

    def __repr__(self):
        """
        Exemple de représentation :
        Difficulté n°1 : Facile
        """
        return "Difficulte n°%d : %s" % (self.id, self.nom)

class Personne(DB.Model, AnonymousUserMixin):
    """
    Table Personne
    """
    id = DB.Column(DB.Integer, primary_key=True)
    est_prof = DB.Column(DB.Boolean)
    prenom = DB.Column(DB.String(100))
    nom = DB.Column(DB.String(100))
    mdp = DB.Column(DB.String(100))
    email = DB.Column(DB.String(255), unique=True)
    active = DB.Column(DB.Boolean, default=False)
    id_image = DB.Column(DB.Integer, DB.ForeignKey('image.id'))

    #Realtion de Personne avec Image (One To Many)
    image = DB.relationship("Image", back_populates="personne")

    #Relation de Personne avec EscapeGame (Many To One)
    modele_escapes = DB.relationship("ModeleEscape", back_populates="personne")

    #Relation de Personne avec Groupe (Many to Many)
    groupes = DB.relationship("Groupe", secondary="association_personne_groupe")

    def get_id(self):
        return self.id

    def get_prenom(self):
        return self.prenom

    def get_nom(self):
        return self.nom

    def get_email(self):
        return self.email

    def set_password(self, password):
        """
        Définit le mdp en le hashant
        """
        self.mdp = generate_password_hash(password, method='pbkdf2:sha512:50000')

    def check_password(self, password):
        """
        Vérifie le hash du mot de passe
        """
        return check_password_hash(self.mdp, password)

    @property
    def is_authenticated(self):
        return True

    def set_change_authentificated(self):
        """
        Change le user.active en son opposé (fonction utilisée pendant connexion / deconnexion )
        """
        self.active = not self.active

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def __repr__(self):
        """
        Exemple de représentation :
        Professeur (1) : Thomas Pinsard, Email : thomas.pinsard@etu.univ-orleans.fr
        """
        if self.est_prof:
            res = "Professeur"
        else:
            res = "Etudiant"
        res += f" id : {self.id}, prénom : {self.prenom}, nom : {self.nom}, email : {self.email}"
        return res


class ModeleEscape(DB.Model):
    """
    Table ModeleEscape
    """
    id = DB.Column(DB.Integer, primary_key=True)
    nom = DB.Column(DB.String(100))
    desc = DB.Column(DB.Text)
    id_personne = DB.Column(DB.Integer, DB.ForeignKey('personne.id'))

    #Relation de ModeleEscape avec Personne (One To Many)
    personne = DB.relationship("Personne", back_populates="modele_escapes")

    #Relation de ModeleEscape avec EscapeGame (Many To One)
    escape_games = DB.relationship("EscapeGame", back_populates="modele_escape")

    #Relation de ModeleEscape avec ModeleSalle (Many To One)
    modele_salles = DB.relationship("ModeleSalle", back_populates="modele_escape")

def get_all_modeleEscapes():
    return ModeleEscape.query.all()


class EscapeGame(DB.Model):
    """
    Table EscapeGame
    """
    id = DB.Column(DB.Integer, primary_key=True)
    date_debut = DB.Column(DB.DateTime)
    date_fin = DB.Column(DB.DateTime)
    id_modele_escape = DB.Column(DB.Integer, DB.ForeignKey('modele_escape.id'))

    #Relation de EscapeGame avec ModeleEscape (One To Many)
    modele_escape = DB.relationship("ModeleEscape", back_populates="escape_games")

    #Relation de EscapeGame avec Salle (Many To One)
    salles = DB.relationship("Salle", back_populates="escape_game")


class ModeleIndice(DB.Model):
    """
    Table ModeleIndice
    """
    id = DB.Column(DB.Integer, primary_key=True)
    contenu = DB.Column(DB.String(100))
    id_image = DB.Column(DB.Integer, DB.ForeignKey('image.id'))
    id_modele_enigme = DB.Column(DB.Integer, DB.ForeignKey('modele_enigme.id'))

    # Relation de ModeleIndice avec Image (One To One)
    image = DB.relationship("Image", back_populates="modele_indice")

    # Relation de ModeleIndice avec ModeleEnigme (One To Many)
    modele_enigme = DB.relationship("ModeleEnigme", back_populates="modele_indices")

    #Relation de ModeleIndice avec Indice (Many To One)
    indices = DB.relationship("Indice", back_populates="modele_indice")

    #Relation de ModeleIndice avec ModeleSalle (Many To Many)
    modele_salles = DB.relationship("ModeleSalle", secondary="association_modele_indice_modele_salle")


class Indice(DB.Model):
    """
    Table Indice
    """
    id = DB.Column(DB.Integer, primary_key=True)
    id_modele_indice = DB.Column(DB.Integer, DB.ForeignKey('modele_indice.id'))
    id_zone = DB.Column(DB.Integer, DB.ForeignKey('zone.id', use_alter=True))
    est_trouve = DB.Column(DB.Boolean)

    #Relation de Indice avec ModeleIndice (One To Many)
    modele_indice = DB.relationship("ModeleIndice", back_populates="indices")

    #Relation de Indice avec Zone (One To Many)
    zone = DB.relationship("Zone", back_populates="indices")

    #Relation de Indice avec Enigme (Many To Many)
    enigmes = DB.relationship("Enigme", secondary="association_indice_enigme")

    #Relation de Indice avec Groupe (Many To Many)
    groupes = DB.relationship("Groupe", secondary="association_groupe_indice")

    def __repr__(self):
        """
        Exemple de représentation :
        Indice n°1 : Utilisez la loi de Bernoulli
        """
        return "Indice n°%d : %s" % (self.id, self.modele_indice.contenu)


class ModeleZone(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    pos_x = DB.Column(DB.Float)
    pos_y = DB.Column(DB.Float)
    hauteur = DB.Column(DB.Float)
    largeur = DB.Column(DB.Float)
    id_modele_salle = DB.Column(DB.Integer, DB.ForeignKey('modele_salle.id'))

    #Relation de ModeleZone avec ModeleSalle (One To Many)
    modele_salle = DB.relationship("ModeleSalle", back_populates="modele_zones")

    #Relation de ModeleZone avec Zone (Many To One)
    zones = DB.relationship("Zone", back_populates="modele_zone")


class Zone(DB.Model):
    """
    Table Zone
    """
    id = DB.Column(DB.Integer, primary_key=True)
    contient_indice = DB.Column(DB.Boolean)
    id_salle = DB.Column(DB.Integer, DB.ForeignKey('salle.id'))
    id_modele_zone = DB.Column(DB.Integer, DB.ForeignKey('modele_zone.id'))

    #Relation de Zone avec ModeleZone (One To Many)
    modele_zone = DB.relationship("ModeleZone", back_populates="zones")

    #Relation de Zone avec Salle (One To Many)
    salle = DB.relationship("Salle", back_populates="zones")

    #Relation de Zone avec Indice (Many To One)
    indices = DB.relationship("Indice", back_populates="zone")

    #Relation de Zone avec IndiceSpeciale (Many To One)
    indices_speciales = DB.relationship("IndiceSpeciale", secondary="association_zone_indice_speciale")

    #Relation de Zone avec Groupe (Many To Many)
    groupes = DB.relationship("Groupe", secondary="association_groupe_zone")

    def __repr__(self):
        """
        Exemple de représentation :
        Zone n°1 : Position(10, 30), Taille(50x50)
        """
        return "Zone n°%d : Position(%d, %d), Taille(%dx%d)" % (self.id, self.modele_zone.pos_x, self.modele_zone.pos_y,
                                                                self.modele_zone.hauteur, self.modele_zone.largeur)


class ModeleSalle(DB.Model):
    """
    Table ModeleSalle
    """
    id = DB.Column(DB.Integer, primary_key=True)
    desc = DB.Column(DB.Text)
    est_libre = DB.Column(DB.Boolean)
    utilise_enigme_spe = DB.Column(DB.Boolean)
    est_personnalisee = DB.Column(DB.Integer)
    ordre_salle = DB.Column(DB.Integer)
    id_image = DB.Column(DB.Integer, DB.ForeignKey('image.id'))
    id_modele_escape = DB.Column(DB.Integer, DB.ForeignKey('modele_escape.id'))

    #Relation de ModeleSalle avec ModeleZone (Many To One)
    modele_zones = DB.relationship("ModeleZone", back_populates="modele_salle")

    #Relation de ModeleSalle avec Image (One To Many)
    image = DB.relationship("Image", back_populates="modele_salle")

    #Relation de ModeleSalle avec Image (One To Many)
    modele_escape = DB.relationship("ModeleEscape", back_populates="modele_salles")

    #Relation de ModeleSalle avec Salle (Many To One)
    salles = DB.relationship("Salle", back_populates="modele_salle")

    #Relation de MoedeleSalle avec ModeleIndice (Many To Many)
    modele_indices = DB.relationship("ModeleIndice", secondary="association_modele_indice_modele_salle")

    #Relation de ModeleSalle avec ModeleEnigme (Many To Many)
    modele_enigmes = DB.relationship("ModeleEnigme", secondary="association_modele_enigme_modele_salle")


class Salle(DB.Model):
    """
    Table Salle
    """
    id = DB.Column(DB.Integer, primary_key=True)
    id_escape_game = DB.Column(DB.Integer, DB.ForeignKey('escape_game.id'))
    id_modele_salle = DB.Column(DB.Integer, DB.ForeignKey('modele_salle.id'))
    id_enigme = DB.Column(DB.Integer, DB.ForeignKey('enigme.id'))

    # Relation de Salle avec Enigme (One To One)
    enigme = DB.relationship("Enigme", back_populates="salle")

    #Relation de Salle avec EscapeGame (One To Many)
    modele_salle = DB.relationship("ModeleSalle", back_populates="salles")

    #Relation de Salle avec EscapeGame (One To Many)
    escape_game = DB.relationship("EscapeGame", back_populates="salles")

    #Relation de Salle avec Zone (Many To One)
    zones = DB.relationship("Zone", back_populates="salle")

    #Relation de Salle avec Groupe (Many To Many)
    groupes = DB.relationship("Groupe", secondary="association_groupe_salle")

    #Relation de Salle avec EnigmeSpeciale (Many To Many)
    enigmes_speciales = DB.relationship("EnigmeSpeciale", secondary="association_enigme_speciale_salle")


    def __repr__(self):
        """
        Exemple de représentation :
        Salle n°1 : Cette salle contient... Libre : Non
        """
        if self.modele_salle.est_libre:
            libre = "Oui"
        else:
            libre = "Non"
        return "Instance de Salle n°%d : %s Libre : %s" % (self.id, self.modele_salle.desc, libre)


class ModeleEnigme(DB.Model):
    """
    Table ModeleEnigme
    """
    id = DB.Column(DB.Integer, primary_key=True)
    intitule = DB.Column(DB.Text)
    solution = DB.Column(DB.Text)
    nb_points = DB.Column(DB.Integer)
    id_image = DB.Column(DB.Integer, DB.ForeignKey('image.id'))

    # Relation de ModeleEnigme avec Image (One To One)
    image = DB.relationship("Image", back_populates="modele_enigme")

    #Relation de ModeleEnigme avec Enigmes (Many To One)
    enigmes = DB.relationship("Enigme", back_populates="modele_enigme")

    # #Relation de ModeleEnigme avec JeuVariable (Many To One)
    # jeu_variables = DB.relationship("JeuVariable", back_populates="modele_enigme")

    #Relation de ModeleEnigme avec ModeleSalle (Many To Many)
    modele_salles = DB.relationship("ModeleSalle", secondary="association_modele_enigme_modele_salle")

    #Relation de ModeleEnigme avec ModeleIndice (Many To One)
    modele_indices = DB.relationship("ModeleIndice", back_populates="modele_enigme")


class Enigme(DB.Model):
    """
    Table Enigme
    """
    id = DB.Column(DB.Integer, primary_key=True)
    est_resolu = DB.Column(DB.Boolean)
    nb_points = DB.Column(DB.Integer)
    id_modele_enigme = DB.Column(DB.Integer, DB.ForeignKey('modele_enigme.id'))
    salle = DB.relationship("Salle", back_populates="enigme")

    #Relation de Enigme avec ModeleEnigme (One To Many)
    modele_enigme = DB.relationship("ModeleEnigme", back_populates="enigmes")

    #Relation de Enigme avec Groupe (Many To Many)
    groupes = DB.relationship("Groupe", secondary="association_enigme_groupe")

    #Relation de Enigme avec Indice (Many To Many)
    indices = DB.relationship("Indice", secondary="association_indice_enigme")


    # def __repr__(self):
    #     """
    #     Exemple de représentation :
    #     Enigme n°1 : IntituleEnigme... solutionEnigme... Points : 5
    #     """
    #     return "Enigme n°%d : %s Solution : %s Points : %d" % (self.id, self.modele_enigme.intitule, self.modele_enigme.solution, self.modele_enigme.nb_points)

class EnigmeSpeciale(DB.Model):
    """
    Table Enigme Spéciale
    """
    id = DB.Column(DB.Integer, primary_key=True)
    intitule = DB.Column(DB.Text)
    solution = DB.Column(DB.Text)
    nb_points = DB.Column(DB.Integer)
    nb_points_groupe_courant = DB.Column(DB.Integer)
    est_resolu_groupe_courant = DB.Column(DB.Boolean)
    id_difficulte = DB.Column(DB.Integer, DB.ForeignKey('difficulte.id'))
    id_theme = DB.Column(DB.Integer, DB.ForeignKey('theme.id'))
    id_image = DB.Column(DB.Integer, DB.ForeignKey('image.id'))

    #Relation de IndiceSpeciale avec Image (One To One)
    image = DB.relationship("Image", back_populates="enigme_speciale")

    # #Relation de EnigmeSpeciale avec Difficulte (One To Many)
    difficulte = DB.relationship("Difficulte", back_populates="enigmes_speciales")

    #Relation de EnigmeSpeciale avec Theme (One To Many)
    theme = DB.relationship("Theme", back_populates="enigmes_speciales")

    #Relation de EnigmeSpeciale avec Salle (Many To Many)
    salles = DB.relationship("Salle", secondary="association_enigme_speciale_salle")

    #Relation de EnigmeSpeciale avec IndiceSpeciale (Many To One)
    indices_speciales = DB.relationship("IndiceSpeciale", back_populates="enigme_speciale")

    #Relation de EnigmeSpeciale avec Groupe (Many To Many)
    groupes = DB.relationship("Groupe", secondary="association_groupe_enigme_speciale")

    def __repr__(self):
        """
        Exemple de représentation :
        Enigme Spéciale n°1 : IntituleEnigme... solutionEnigme... Points : 5
        """
        return "Enigme Spéciale n°%d : %s Solution : %s Points : %d" % (self.id, self.intitule, self.solution, self.nb_points)

class IndiceSpeciale(DB.Model):
    """
    Table Indice Spéciale
    """
    id = DB.Column(DB.Integer, primary_key=True)
    contenu = DB.Column(DB.String(100))
    est_trouve = DB.Column(DB.Boolean)
    id_enigme_speciale = DB.Column(DB.Integer, DB.ForeignKey('enigme_speciale.id'))
    id_image = DB.Column(DB.Integer, DB.ForeignKey('image.id'))

    #Relation de IndiceSpeciale avec EnigmeSpeciale (One To Many)
    enigme_speciale = DB.relationship("EnigmeSpeciale", back_populates="indices_speciales")

    #Relation de Indice avec Zone (Many To Many)
    zones = DB.relationship("Zone", secondary="association_zone_indice_speciale")

    #Relation de IndiceSpeciale avec Groupe (Many To Many)
    groupes = DB.relationship("Groupe", secondary="association_groupe_indice_speciale")

    #Relation de IndiceSpeciale avec Image (One To One)
    image = DB.relationship("Image", back_populates="indice_speciale")

    def __repr__(self):
        """
        Exemple de représentation :
        Indice Spéciale n°1 : IntituleEnigme... solutionEnigme... Points : 5
        """
        return "Indice Spéciale n°%d : %s : Enigme Speciale n°%d " % (self.id, self.contenu, self.id_enigme_speciale)



# class JeuVariable(DB.Model):
#     """
#     Table JeuVariable
#     """
#     id = DB.Column(DB.Integer, primary_key=True)
#     id_modele_enigme = DB.Column(DB.Integer, DB.ForeignKey('modele_enigme.id'))
#
#     #Relation de JeuVariable avec ModeleEnigme (One To Many)
#     modele_enigme = DB.relationship("ModeleEnigme", back_populates="jeu_variables")
#
#     #Relation de JeuVariable avec Variable (Many To Many)
#     variables = DB.relationship("Variable", secondary="association_jeu_variable_variable")


# class Variable(DB.Model):
#     """
#     Table Variable
#     """
#     id = DB.Column(DB.Integer, primary_key=True)
#     nom = DB.Column(DB.String(100))
#     valeur = DB.Column(DB.Integer)
#
#     #Relation de Variable avec JeuVariable (Many To Many)
#     jeu_variables = DB.relationship("JeuVariable", secondary="association_jeu_variable_variable")
