function importerImages(){
  images = document.getElementById('images').files
  if(images){
    document.getElementById('loading').style.display='block';

    input1 = document.createElement("input");
    input1.setAttribute("type", "hidden");
    input1.setAttribute("name", "nbImages");
    input1.setAttribute("id", "nbImages");
    input1.setAttribute("value", images.length);
    document.getElementById("input-images").appendChild(input1);
    let cpt = images.length;
    for (i = 0; i < images.length; i++){
      if (images[i]['type'].split('/')[0] === 'image'){
        
        let reader = new FileReader();
        let input = document.createElement("input");
        reader.addEventListener('load', function(){
          input.setAttribute("value", this.result);
        });
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "hiddenImages" + i);
        input.setAttribute("id", "hiddenImages" + i);
        document.getElementById("input-images").appendChild(input);
        reader.readAsDataURL(images[i]);
        reader.addEventListener('loadend', function(){
          cpt --;
          envoie(cpt);
        });
      }else{
        alert('Veuillez choisir une image');
      }
    }
  }
}

function envoie(cpt){
  if (cpt == 0){
    document.getElementById('form-images').submit();
  }
}


function importerEnigme(){
  if(document.getElementById('fichier').files){
    if(document.getElementById('fichier').files[0].name.endsWith(".xls")){
      document.getElementById('form-fichier').submit();
      document.getElementById('loading').style.display='block';
    }
  }
}